package com.att.parser.tiffparser;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.util.Log;

import org.apache.commons.imaging.common.IImageMetadata;
import org.apache.commons.imaging.formats.tiff.TiffField;

        import java.io.File;
        import java.io.IOException;
        import java.util.List;

        import org.apache.commons.imaging.ImageReadException;
        import org.apache.commons.imaging.Imaging;
        import org.apache.commons.imaging.common.ImageMetadata;
        import org.apache.commons.imaging.common.RationalNumber;
        import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
        import org.apache.commons.imaging.formats.tiff.TiffField;
        import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
        import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
        import org.apache.commons.imaging.formats.tiff.constants.GpsTagConstants;
        import org.apache.commons.imaging.formats.tiff.constants.TiffTagConstants;
        import org.apache.commons.imaging.formats.tiff.taginfos.TagInfo;

public class MetadataExample {
    final static String TAG = "TIFF";
    static String output;
    public static void metadataExample(final File file) throws ImageReadException,
            IOException {
        // get all metadata stored in EXIF format (ie. from JPEG or TIFF).
        final IImageMetadata metadata = Imaging.getMetadata(file);

        // Log.d(metadata);

        if (metadata instanceof TiffImageMetadata) {
            final TiffImageMetadata tiffImageMetadata = (TiffImageMetadata) metadata;

            // Jpeg EXIF metadata is stored in a TIFF-based directory structure
            // and is identified with TIFF tags.
            // Here we look for the "x resolution" tag, but
            // we could just as easily search for any other tag.
            //
            // see the TiffConstants file for a list of TIFF tags.

            Log.d(TAG,"file: " + file.getPath());

            // print out various interesting EXIF tags.
            printTagValue(tiffImageMetadata, TiffTagConstants.TIFF_TAG_XRESOLUTION);
            printTagValue(tiffImageMetadata, TiffTagConstants.TIFF_TAG_DATE_TIME);
            printTagValue(tiffImageMetadata,
                    ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL);
            printTagValue(tiffImageMetadata, ExifTagConstants.EXIF_TAG_DATE_TIME_DIGITIZED);
            printTagValue(tiffImageMetadata, ExifTagConstants.EXIF_TAG_ISO);
            printTagValue(tiffImageMetadata,
                    ExifTagConstants.EXIF_TAG_SHUTTER_SPEED_VALUE);
            printTagValue(tiffImageMetadata,
                    ExifTagConstants.EXIF_TAG_APERTURE_VALUE);
            printTagValue(tiffImageMetadata,
                    ExifTagConstants.EXIF_TAG_BRIGHTNESS_VALUE);
            printTagValue(tiffImageMetadata,
                    GpsTagConstants.GPS_TAG_GPS_LATITUDE_REF);
            printTagValue(tiffImageMetadata, GpsTagConstants.GPS_TAG_GPS_LATITUDE);
            printTagValue(tiffImageMetadata,
                    GpsTagConstants.GPS_TAG_GPS_LONGITUDE_REF);
            printTagValue(tiffImageMetadata, GpsTagConstants.GPS_TAG_GPS_LONGITUDE);

            Log.d(TAG,"\n");

            // simple interface to GPS data
            final TiffImageMetadata.GPSInfo gpsInfo = tiffImageMetadata.getGPS();
            if (null != gpsInfo) {
                final String gpsDescription = gpsInfo.toString();
                final double longitude = gpsInfo.getLongitudeAsDegreesEast();
                final double latitude = gpsInfo.getLatitudeAsDegreesNorth();

                Log.d(TAG,"    " + "GPS Description: "
                        + gpsDescription);
                Log.d(TAG,"    "
                        + "GPS Longitude (Degrees East): " + longitude);
                Log.d(TAG,"    "
                        + "GPS Latitude (Degrees North): " + latitude);
            }

            // more specific example of how to manually access GPS values
            final TiffField gpsLatitudeRefField = tiffImageMetadata.findField(
                    GpsTagConstants.GPS_TAG_GPS_LATITUDE_REF);
            final TiffField gpsLatitudeField = tiffImageMetadata.findField(
                    GpsTagConstants.GPS_TAG_GPS_LATITUDE);
            final TiffField gpsLongitudeRefField = tiffImageMetadata.findField(
                    GpsTagConstants.GPS_TAG_GPS_LONGITUDE_REF);
            final TiffField gpsLongitudeField = tiffImageMetadata.findField(
                    GpsTagConstants.GPS_TAG_GPS_LONGITUDE);
            if (gpsLatitudeRefField != null && gpsLatitudeField != null &&
                    gpsLongitudeRefField != null &&
                    gpsLongitudeField != null) {
                // all of these values are strings.
                final String gpsLatitudeRef = (String) gpsLatitudeRefField.getValue();
                final RationalNumber gpsLatitude[] = (RationalNumber[]) (gpsLatitudeField.getValue());
                final String gpsLongitudeRef = (String) gpsLongitudeRefField.getValue();
                final RationalNumber gpsLongitude[] = (RationalNumber[]) gpsLongitudeField.getValue();

                final RationalNumber gpsLatitudeDegrees = gpsLatitude[0];
                final RationalNumber gpsLatitudeMinutes = gpsLatitude[1];
                final RationalNumber gpsLatitudeSeconds = gpsLatitude[2];

                final RationalNumber gpsLongitudeDegrees = gpsLongitude[0];
                final RationalNumber gpsLongitudeMinutes = gpsLongitude[1];
                final RationalNumber gpsLongitudeSeconds = gpsLongitude[2];

                // This will format the gps info like so:
                //
                // gpsLatitude: 8 degrees, 40 minutes, 42.2 seconds S
                // gpsLongitude: 115 degrees, 26 minutes, 21.8 seconds E

                Log.d(TAG,"    " + "GPS Latitude: "
                        + gpsLatitudeDegrees.toDisplayString() + " degrees, "
                        + gpsLatitudeMinutes.toDisplayString() + " minutes, "
                        + gpsLatitudeSeconds.toDisplayString() + " seconds "
                        + gpsLatitudeRef);
                Log.d(TAG,"    " + "GPS Longitude: "
                        + gpsLongitudeDegrees.toDisplayString() + " degrees, "
                        + gpsLongitudeMinutes.toDisplayString() + " minutes, "
                        + gpsLongitudeSeconds.toDisplayString() + " seconds "
                        + gpsLongitudeRef);

            }

            Log.d(TAG,"\n");

            List<? extends IImageMetadata.IImageMetadataItem> items = tiffImageMetadata.getItems();
            for (int i = 0; i < items.size(); i++) {
                final IImageMetadata.IImageMetadataItem item = items.get(i);
                Log.d(TAG,"    " + "item: " + item);
            }

            Log.d(TAG,"\n");
        } else {
            Log.d(TAG,"Not a Tiff:"+metadata);
        }
    }

    private static void printTagValue(final TiffImageMetadata jpegMetadata,
                                      final TagInfo tagInfo) throws ImageReadException {
        final TiffField field = jpegMetadata.findField(tagInfo);
        if (field == null) {
            Log.d(TAG,tagInfo.name + ": " + "Not Found.");
        } else {
            Log.d(TAG,tagInfo.name + ": "
                    + field.getValueDescription());
        }
    }

}

