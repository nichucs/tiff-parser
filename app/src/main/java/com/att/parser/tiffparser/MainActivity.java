package com.att.parser.tiffparser;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.apache.commons.imaging.ImageReadException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_PICK_FILE = 1;
    Button browse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        browse = (Button) findViewById(R.id.browse);

        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> extentions = new ArrayList<>();
                extentions.add("tif");
                extentions.add("tiff");
                extentions.add("TIF");
                extentions.add("TIFF");
                Intent intent = new Intent(MainActivity.this, FilePicker.class);
                intent.putStringArrayListExtra(FilePicker.EXTRA_ACCEPTED_FILE_EXTENSIONS, extentions);
                startActivityForResult(intent, REQUEST_PICK_FILE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_PICK_FILE:

                    if (data.hasExtra(FilePicker.EXTRA_FILE_PATH)) {

                        final File selectedFile = new File
                                (data.getStringExtra(FilePicker.EXTRA_FILE_PATH));
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        MetadataExample.metadataExample(selectedFile);
                                    } catch (ImageReadException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },500);
                    }
                    break;
            }
        }
    }
}
